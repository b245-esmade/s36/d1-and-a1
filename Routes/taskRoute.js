const express = require('express');
const router = express.Router();

const taskController =require('../Controllers/taskControllers.js')

/*Routes*/

//Route for GET ALL
router.get('/get', taskController.getAll)



//Route for POST/Add task
router.post('/addTask', taskController.createTask);

module.exports = router;

router.delete('/deleteTask/:id', taskController.deleteTask)



//Route to GET Specific Task
router.get('/:id', taskController.getSpecTask)




//Route to UPDATE/PUT Specific Task
router.put('/:id/complete', taskController.updateTaskStat)
