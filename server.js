
// Express.js - Modules and Parameterized Routes

// Separation of Concerns
	/*
		-better code readablity
		-improve scalability
		-better code maintainability
	*/

	/*
		Separate the following:
		Models
			-Schema
		Controllers
			-function
		Routes
			-routing
	*/

	/* Connection

		 server > model/schema > controller > route > connect server

		1.Model should be connected to the controller
		2.Controller should be connected to the Routes
		3.Route should be connected to the server/application

	*/


 // JS Modules
	// self-contained units of functionality that can be shared and reuse
	// "module.exports"




	const express = require('express');
	const mongoose = require('mongoose');

	const taskRoute = require('./Routes/taskRoute.js')

	// set server
	const app = express();

	// set port
	const port = 3001;


	// MongooseDB connection
	mongoose.connect('mongodb+srv://admin:admin@batch245-esmade.c4tetod.mongodb.net/s35-discussion?retryWrites=true&w=majority',
		{
			// to avoid error/impact to future changes while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true
		})



	// contain mongoose to variable to check connection
	let db = mongoose.connection;

	// error catcher
	db.on('error', console.error.bind(console,'Connection Error!'))

	// confirmation
	db.once('open',() => console.log('We are now connected to the cloud!'))



	// Middleware

	app.use(express.json());
	app.use(express.urlencoded({extended:true}));


	// Routing

	app.use('/tasks', taskRoute)


	app.listen(port, ()=> console.log(`Server is running at port ${port}!`))