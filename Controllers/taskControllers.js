const Task = require('../Models/task.js')



/*Controllers and Functions*/

// GET all the task in our database
module.exports.getAll = (request, response) => {

	Task.find({}).then(result => {
		// to capture the result of the find method
			return response.send(result)
	})
		// to capture the error when find method is executed
	.catch(error => {
		return response.send(error);
	})
}



// CREATE/POST task to our database
module.exports.createTask =(request,response) => {
	const input = request.body;


	Task.findOne({name: input.name})
	.then(result => {
		if(result !== null){
			return response.send('The task is already existing!')
		}else {
			let newTask = new Task ({
				name: input.name
			});

			newTask.save().then(save=>{
				return response.send ('The task is successfully added!')
			}).catch(error => {
				return response.send(error)
			})
		}

	})


	.catch(error => {
		return response.send(error)
	})
}




// DELETE task to our database

module.exports.deleteTask = (request,response) =>{
	let idToBeDeleted = request.params.id;

	// findByIdAndRemove - to find the document that contains the id and then delete the document

	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
};



// GET specific task to our database

module.exports.getSpecTask = (request,response)=>{
	let taskId = request.params.id;

	Task.findById(taskId)
	.then(result => {
		// to capture the result of the find method
			return response.send(result)
	})
		// to capture the error when find method is executed
	.catch(error => {
		return response.send(error);
	})
};


// UPDATE status of specific task to our database

module.exports.updateTaskStat = (request,response) =>{
	let taskId = request.params.id;

	Task.findByIdAndUpdate(taskId, {status:'Completed'})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})

}