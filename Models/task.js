
// Schema

const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Name is required']
	},
	status: {
		type: String,
		default: 'pending'
	}
});




// Model
//to export					// task Model
module.exports = mongoose.model('Task', taskSchema);